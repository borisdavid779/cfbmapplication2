package boris.zambrano.pm.facci.cfbm_application.SQlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import boris.zambrano.pm.facci.cfbm_application.Tablas.Proceso;

public class SqliteHelperProceso extends SQLiteOpenHelper {

    //DATABASE NAME
    public static final String DATABASE_NAME = "proceso.db";

    //DATABASE VERSION
    public static final int DATABASE_VERSION = 1;

    public static final String TABLA_PROCESO="proceso";
    public static final String ID="id";
    public static final String FECHA="fecha";
    public static final String RAZON_SOCIAL="razon_social";
    public static final String CANTIDAD="cantidad";
    public static final String PRECIO="precio";
    public static final String TOTAL="total";
    public static final String DESCUENTO="descuento";
    public static final String CLIENTE="cliente";


    public static final String CREATE_TABLE_PROCESO="CREATE TABLE "+ TABLA_PROCESO +" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+FECHA+" TEXT, "+CLIENTE+" TEXT, "+RAZON_SOCIAL+" TEXT, " +
            ""+CANTIDAD+" TEXT, "+PRECIO+" TEXT, "+TOTAL+" TEXT, "+DESCUENTO+" TEXT)";

    public SqliteHelperProceso(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //ejecutamos la sentencia antrior para crear tabla
        sqLiteDatabase.execSQL(CREATE_TABLE_PROCESO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int versionold, int versionew) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS proceso");
        onCreate(sqLiteDatabase);
    }

    public void addData(Proceso proceso){
        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put username in  @values
        values.put(ID, proceso.id);
        values.put(FECHA, proceso.fecha);
        values.put(RAZON_SOCIAL, proceso.razon_social);
        values.put(CANTIDAD, proceso.cantidad);
        values.put(PRECIO, proceso.precio);
        values.put(TOTAL, proceso.total);
        values.put(DESCUENTO, proceso.descuento);
        values.put(CLIENTE, proceso.cliente);
        long insert = db.insert(TABLA_PROCESO, null, values);
    }


}

