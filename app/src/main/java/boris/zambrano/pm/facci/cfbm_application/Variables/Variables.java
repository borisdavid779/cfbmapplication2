package boris.zambrano.pm.facci.cfbm_application.Variables;

public class Variables {

    //campos tabla proceso
    public static final String TABLA_PROCESO="proceso";
    public static final String ID="id";
    public static final String FECHA="fecha";
    public static final String RAZON_SOCIAL="razon_social";
    public static final String CANTIDAD="cantidad";
    public static final String PRECIO="precio";
    public static final String TOTAL="total";
    public static final String DESCUENTO="descuento";
    public static final String IVA="iva";
    public static final String CLIENTE="cliente";

    //Script de la sentencia que crea la tabla
    public static final String CREATE_TABLE_PROCESO="CREATE TABLE "+ TABLA_PROCESO +" ("+ID+" INTEGER, "+FECHA+" TEXT, "+CLIENTE+" TEXT, "+RAZON_SOCIAL+" TEXT, " +
            ""+CANTIDAD+" TEXT, "+PRECIO+" TEXT, "+TOTAL+" TEXT, "+DESCUENTO+" TEXT, "+IVA+" TEXT)";
}
