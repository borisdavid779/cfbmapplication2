package boris.zambrano.pm.facci.cfbm_application.Tablas;

public class Proceso {
    public Integer id;
    public String fecha;
    public String cliente;
    public String razon_social;
    public String cantidad;
    public String precio;
    public String total;
    public String descuento;

    public Proceso(Integer id, String fecha, String cliente, String razon_social,
                   String cantidad, String precio, String total,
                   String descuento) {
        this.id = id;
        this.fecha = fecha;
        this.cliente = cliente;
        this.razon_social = razon_social;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
        this.descuento = descuento;
    }
}
