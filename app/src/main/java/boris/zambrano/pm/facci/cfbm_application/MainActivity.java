package boris.zambrano.pm.facci.cfbm_application;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import boris.zambrano.pm.facci.cfbm_application.SQlite.SqliteHelperProceso;
import boris.zambrano.pm.facci.cfbm_application.Tablas.Proceso;

public class MainActivity extends AppCompatActivity {

    private Proceso proceso;
    private AppCompatButton ingreso;
    private AppCompatButton consulta;
    SqliteHelperProceso sqliteHelperProceso;
    private EditText editId, editfecha, editcliente, editrazon_social, editcantidad, editprecio, editdescuento, edittotal;
    double iva = 0.12;
    double v_total;
    double valor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sqliteHelperProceso = new SqliteHelperProceso(this);
        ingreso = (AppCompatButton) findViewById(R.id.ingreso);
        consulta = (AppCompatButton) findViewById(R.id.consulta);
        editprecio = findViewById(R.id.editprecio);
        editId = findViewById(R.id.editId);
        editfecha = findViewById(R.id.editfecha);
        editcliente = findViewById(R.id.editcliente);
        editrazon_social = findViewById(R.id.editrazon_social);
        editcantidad = findViewById(R.id.editcantidad);
        editdescuento = findViewById(R.id.editdescuento);
        edittotal = findViewById(R.id.edittotal);
        ingreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editprecio.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "COMPLETE LOS CAMPOS", Toast.LENGTH_SHORT).show();
                } else {
                    if (sqliteHelperProceso != null) {
                        String id = editId.getText().toString();
                        String fecha = editfecha.getText().toString();
                        String cliente = editcliente.getText().toString();
                        String razon_social = editrazon_social.getText().toString();
                        String cantidad = editcantidad.getText().toString();
                        String precio = editprecio.getText().toString();
                        String descuento = editdescuento.getText().toString();
                        v_total = Double.parseDouble(editcantidad.getText().toString()) * Double.parseDouble(editprecio.getText().toString());
                        valor = v_total * iva;
                        v_total = v_total + valor;
                        String total = String.valueOf(v_total);
                        edittotal.setText(total);
                        sqliteHelperProceso.addData(new Proceso(null, fecha, cliente, razon_social, cantidad, precio, descuento, total));
                        Toast.makeText(MainActivity.this, "Total a pagar: " + v_total, Toast.LENGTH_SHORT).show();
                        Snackbar.make(ingreso, getString(R.string.success), Snackbar.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MainActivity.this, "Conexion fallida", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        /*
        consulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editId.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "INGRESE EL CODIGO", Toast.LENGTH_SHORT).show();
                }else{
                    if(sqliteHelperProceso != null){

                        //Toast.makeText(MainActivity.this, "jijijijij", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(MainActivity.this, "Conexion fallida", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        consulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SqliteHelperProceso conexion = new SqliteHelperProceso(getApplicationContext());
                SQLiteDatabase db = conexion.getWritableDatabase();
                String id = editId.getText().toString();
                String[] campos ={Variables.CANTIDAD, Variables.FECHA, Variables.RAZON_SOCIAL,
                Variables.CANTIDAD, Variables.PRECIO, Variables.TOTAL, Variables.DESCUENTO, Variables.IVA,
                Variables.CLIENTE};
                if (!id.isEmpty()) {
                    //Cursor fila = db.rawQuery("select total, cantidad from proceso.db where id =" + id, null);
                    Cursor fila  = db.query(Variables.TABLA_PROCESO,campos,Variables.ID+"=?",id,null);
                    if (fila.moveToFirst()) {
                        edittotal.setText(fila.getString(0));
                        editcantidad.setText(fila.getString(1));
                        db.close();
                    } else {
                        Toast.makeText(getApplicationContext(), "NO EXISTE", Toast.LENGTH_LONG).show();
                        db.close();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "INTRODUCIR CODIGO", Toast.LENGTH_LONG).show();
                }
            }
        });
        */
    }
}
